/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package undotheweb;

/**
 *
 * @author alexxx
 */
public class MathEngine {
            
    protected Vector getVector(Point start, Point end) {
        return new Vector(
            end.x - start.x,
            end.y - start.y,
            0
        );
    }
    
    protected Vector getVectorsMultiply(Vector a, Vector b) {
        return new Vector(
            a.y*b.z - a.z*b.y,
            a.z*b.x - a.x*b.z,
            a.x*b.y - a.y*b.x
        );
    }
    
    protected boolean isSameSigns(double first, double second) {
        return (first > 0 && second > 0) || (first < 0 && second < 0);
    }

    protected boolean pointBetweenByX(Point a, Point b, Point c) {
        return ((a.x < b.x) && (b.x < c.x)) || ((c.x < b.x) && (b.x < a.x));
    }
    
    protected boolean isPointOnLine(Line line, Point point) {
        Vector a = getVectorsMultiply(getVector(line.start, line.end), getVector(line.start, point));
        return a.z == 0 && pointBetweenByX(line.start, point, line.end);
    }
    
    public boolean checkLinesIntersects(Line a, Line b){
        Vector aFirst = getVectorsMultiply(getVector(a.start, b.start), getVector(a.start, b.end));
        Vector aSecond = getVectorsMultiply(getVector(a.end, b.start), getVector(a.end, b.end));
        Vector bFirst = getVectorsMultiply(getVector(b.start, a.start), getVector(b.start, a.end));
        Vector bSecond = getVectorsMultiply(getVector(b.end, a.start), getVector(b.end, a.end));
        if (aFirst.z == 0 || aSecond.z == 0 || bFirst.z == 0 || bSecond.z == 0) {
            return (isPointOnLine(a, b.start) || isPointOnLine(a, b.end) ||
                    isPointOnLine(b, a.start) || isPointOnLine(b, a.end));
        }
        boolean aResult = !isSameSigns(aFirst.z, aSecond.z);
        boolean bResult = !isSameSigns(bFirst.z, bSecond.z);
        return aResult && bResult;
    }
}
