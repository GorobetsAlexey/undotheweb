/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package undotheweb;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author alexxx
 */
public class Node {
    
    private ArrayList<Line> sides;
    private Point center;
    private int width = 30;
    private ArrayList<Line> links;
    public int index;
            
    public Node(double x, double y) {
        this.links = new ArrayList<Line>();
        this.center = new Point(x, y);
        this.createSides();
    }
    
    public Node(Point center) {
        this.links = new ArrayList<Line>();
        this.center = center.clone();
        this.createSides();
    }
    
    private void createSides() {
        this.sides = new ArrayList<Line>();        
        double startX = this.center.x - this.width/2;
        double endX = startX + this.width;
        double startY = this.center.y - this.width/2;
        double endY = startY + this.width;
        double startHalfY = this.center.y - this.width/4;
        double endHalfY = this.center.y + this.width/4;
        
        this.sides.add(new Line(startX, startHalfY, startX, endHalfY));
        this.sides.add(new Line(startX, endHalfY, this.center.x, endY));
        this.sides.add(new Line(this.center.x, endY, endX, endHalfY));
        this.sides.add(new Line(endX, endHalfY, endX, startHalfY));
        this.sides.add(new Line(endX, startHalfY, this.center.x, startY));
        this.sides.add(new Line(this.center.x, startY, startX, startHalfY));
    }
    
    public void addLink(Line link) {
        addLink(link, true);
    }
    
    public void addLink(Line link, boolean linkToEnd) {
        if (linkToEnd) {
            link.end = this.center.clone();
        } else {
            link.start = this.center.clone();
        }
        this.links.add(link);
    }
    
    public Line createLink() {
        Line link = new Line(this.center.clone(), new Point(0, 0));
        this.links.add(link);
        return link;
    }
    
    public ArrayList<Line> getLinks() {
        return this.links;
    }
    
    public boolean hasIntersections(ArrayList<Line> lines) {
        Iterator<Line> linesIterator = lines.iterator();
        MathEngine engine = new MathEngine();
        while(linesIterator.hasNext()) {
            Line line = linesIterator.next();
            boolean nodeLink = this.links.indexOf(line) >= 0;
            Iterator<Line> linksIterator = this.links.iterator();
            Iterator<Line> sidesIterator = this.sides.iterator();
            while(linksIterator.hasNext()) {
                Line link = linksIterator.next();
                if (engine.checkLinesIntersects(line, link)) {
                    return true;
                }
            }
            if (!nodeLink) {
                while(sidesIterator.hasNext()) {
                    Line side = sidesIterator.next();                
                    if (engine.checkLinesIntersects(line, side)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
   
    protected void drawLine(Line line, Graphics g, boolean drawThickLine) {
        g.drawLine((int)line.start.x, (int)line.start.y, (int)line.end.x, (int)line.end.y);
        if (drawThickLine) {
            g.drawLine((int)line.start.x-1, (int)line.start.y-1, (int)line.end.x-1, (int)line.end.y-1);
        }
    }
    
    protected void drawLine(Line line, Graphics g) {
        this.drawLine(line, g, false);
    }
    
    public void drawLinks(Graphics g) {
        Iterator<Line> linksIterator = this.links.iterator();
        while(linksIterator.hasNext()) {
            Line link = linksIterator.next();
            drawLine(link, g);
        }
    }
    public void drawNode(Graphics g, boolean isSelected) {
        g.setColor(Color.white);
        g.fillRect((int)this.center.x-this.width/2, (int)this.center.y-this.width/2, this.width, (int)this.width);
        if (isSelected) {
            g.setColor(new Color(66, 197, 41));
        } else {
            g.setColor(Color.gray);
        }
        Iterator<Line> sidesIterator = this.sides.iterator();
        while(sidesIterator.hasNext()) {
            Line side = sidesIterator.next();
            drawLine(side, g, isSelected);
        }        
        g.setColor(Color.black);
    }
    
    public boolean isClickedOnNode(int x, int y) {
        int startX = (int)this.center.x - this.width/2;
        int startY = (int)this.center.y - this.width/2;
        int endX = startX + this.width;
        int endY = startY + this.width;
        return (startX < x && x < endX && startY < y && y < endY);
    }
    
   public void move(int x, int y) {
       Point newCenter =  new Point(x, y);
       Iterator<Line> iterator = this.links.iterator();
       while(iterator.hasNext()) {
           Line line = iterator.next();
           if(line.start.equals(this.center)) {
               line.start = newCenter.clone();
           } else {
               line.end = newCenter.clone();
           }
       }
       this.center = newCenter.clone();
       this.createSides();
   }
   
   public void move(Point position) {
       this.move((int)position.x, (int)position.y);
   }
}

