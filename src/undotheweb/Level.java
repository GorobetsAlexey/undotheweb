/*
 * @class Level 
 * Нужен для:
 * 1) создания изначально запутанного графа
 * 2) Сохранения/загрузки
 * 3) Проверки решения
 */
package undotheweb;
import java.util.concurrent.ThreadLocalRandom;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author alexxx
 */
public class Level {
    
    private ArrayList<Node> nodes;
    private int borderOffset = 25;
    private int columnWidth;
    private int rowHeight;
    private int totalWidth;
    private int totalHeigth;
    public Node currentNode;
    
    public Level(int nodesCount, int width, int heigth) {
        if(nodesCount < 3 || nodesCount > 100) {
            throw new IllegalArgumentException();
        }
        this.nodes = new ArrayList<Node>();
        this.totalHeigth = heigth;
        this.totalWidth = width;
        this.columnWidth = (width - this.borderOffset*2) / this.getNodesInRowCount(nodesCount);
        this.rowHeight = (heigth - this.borderOffset*2) / this.getNodesInRowCount(nodesCount);
        this.generateNodes(nodesCount);
    }
    
    protected void generateNodes(int count) {
        java.util.logging.Logger.getLogger(GameFrame.class.getName()).log(java.util.logging.Level.SEVERE, "generateNodes");
        for (int i = 0; i < count; i++) {
            Point center = this.getNodeCenter(i, count);
            Node node = new Node(center);
            node.index = i;
            this.nodes.add(node);
        }
        this.linkNodes();
        int tries = 0;
        {
            this.randomizeNodesPosititon();
            tries++;
        } while(this.isSolved() && tries < 10);
    
    }
    
    protected void linkNodes() {
        this.linkNodesByRows();
        this.linkNodesByColumns();
        this.linkNodesCrossRows();
    }
    
    protected void linkNodesByRows() {
        int rowLength = this.getNodesInRowCount(this.nodes.size());
        for (int i = 0; i < this.nodes.size()-1; i++) {
            if (i/rowLength != (i+1)/rowLength) {
                continue;                
            }
            Node first = this.nodes.get(i);
            Node second = this.nodes.get(i+1);
            Line link = first.createLink();
            second.addLink(link);
        }
    }
    
    protected void linkNodesByColumns() {
        int rowLength = this.getNodesInRowCount(this.nodes.size());
        for (int i = 0; i < this.nodes.size() - rowLength; i++) {
            Node first = this.nodes.get(i);
            Node second = this.nodes.get(i+rowLength);
            Line link = first.createLink();
            second.addLink(link);
        }
    }
    
    protected void linkNodesCrossRows() {
        int rowLength = this.getNodesInRowCount(this.nodes.size());
        for (int i = 1; i < this.nodes.size()-rowLength+1; i++) {
            Node first = this.nodes.get(i);
            Node second = this.nodes.get(i+rowLength-1);
            Line link = first.createLink();
            second.addLink(link);
        }
    }
    
    protected Point getNodeCenter(int nodeIndex, int totalNodesCount) {
        int row = this.getNodeRowByIndex(nodeIndex, totalNodesCount);
        int column = this.getNodeColumnByIndex(nodeIndex, totalNodesCount);
        return new Point(
                this.borderOffset + this.columnWidth*column,
                this.borderOffset + this.rowHeight*row
        );                
    }
    
    protected int getNodeRowByIndex(int nodeIndex, int totalNodesCount) {
        int nodesInRow = this.getNodesInRowCount(totalNodesCount);
        int mod = nodeIndex % nodesInRow;
        int row = nodeIndex/nodesInRow;
        return row;
    }
    
    protected int getNodeColumnByIndex(int nodeIndex, int totalNodesCount) {
        int nodesInRow = this.getNodesInRowCount(totalNodesCount);
        return nodeIndex % nodesInRow;
    }
    
    protected int getNodesInRowCount(int totalNodesCount) {
        int root = (int) Math.sqrt(totalNodesCount);
        if (totalNodesCount > root*root) {
            root++;
        }
        return root;
    }
    
    protected void randomizeNodesPosititon() {
        Iterator<Node> iterator = this.nodes.iterator();
        Point minStartPosition = new Point(
            this.totalWidth/4,
            this.totalHeigth/4
        );
        Point maxStartPosition = new Point(
            3*this.totalWidth/4,
            3*this.totalHeigth/4
        );
        while(iterator.hasNext()) {
            Node node = iterator.next();
            Point newPosition = this.getRandomPosition(minStartPosition, maxStartPosition);
            node.move(newPosition);      
        }
    }
    
    protected Point getRandomPosition(Point min, Point max) {
        return new Point(
            (int)ThreadLocalRandom.current().nextInt((int)min.x, (int)max.x + 1),
            (int)ThreadLocalRandom.current().nextInt((int)min.y, (int)max.y + 1)
        );
    }

    public int getNodesCount() {
        return this.nodes.size();
    }
    
    public boolean hasIntersectios() {
        Iterator<Node> nodesIterator = this.nodes.iterator();
        while(nodesIterator.hasNext()) {
            Node node = nodesIterator.next();
            Iterator<Node> compareIterator = this.nodes.iterator();
            while(compareIterator.hasNext()) {
                Node currentNode = compareIterator.next();
                if (currentNode.equals(node)) {
                    continue;
                }
                if (currentNode.hasIntersections(node.getLinks())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean isSolved() {
        return !this.hasIntersectios();
    }
    
    public Node getNode(int x, int y) {
        Iterator<Node> iterator = this.nodes.iterator();
        while(iterator.hasNext()) {
            Node node = iterator.next();
            if (node.isClickedOnNode(x, y)) {
                return node;
            }
        }
        return null;
    }

    public void draw(Graphics g) {
        Iterator<Node> nodesIterator = this.nodes.iterator();
        while(nodesIterator.hasNext()) {
            Node node = nodesIterator.next();
            node.drawLinks(g);
        }
        nodesIterator = this.nodes.iterator();
        while(nodesIterator.hasNext()) {
            Node node = nodesIterator.next();
            node.drawNode(g, node.equals(this.currentNode));
        }
    }
    
    
}

