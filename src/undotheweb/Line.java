/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package undotheweb;

/**
 *
 * @author alexxx
 */
public class Line {
    
    public Point start;
    public Point end;
    
    public Line (double x1, double y1, double x2, double y2) {
        start = new Point(x1, y1);
        end = new Point(x2, y2);
    }
    
     public Line (Point start, Point end) {
        this.start = start.clone();
        this.end = end.clone();
    }
     
    @Override
    public String toString() {
        return this.start.toString() + "; " + this.end.toString();
    }
}
