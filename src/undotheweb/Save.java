/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package undotheweb;

/**
 *
 * @author alexxx
 */
import java.io.Serializable;

/**
 *
 * @author alexxx
 */
public class Save implements Serializable{
    public long score;
    public int nodes;
    public String champion;
    
    public Save (int nodes, long score, String champion) {
        this.nodes = nodes;
        this.score = score;
        this.champion = champion;
    }
}
