/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package undotheweb;

import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author alexxx
 */
public class RecordsTableModel extends AbstractTableModel {
    
    private ArrayList<Save> records;
    
    public RecordsTableModel(HashMap<Integer, Save> data) {
        this.records = new ArrayList<Save>(data.values());
    }
    
    @Override
    public String getColumnName(int col) {
        switch(col){
            case 0:
                return "Вершин";
            case 1:
                return "Секунд";
            case 2:
                return "Имя";
        }
        return "";
    }

    @Override
    public int getRowCount() { 
        return this.records.size();
    }
    
    @Override
    public int getColumnCount() { 
        return 3;
    }
    
    @Override
    public Object getValueAt(int row, int col) {
        Save rowData = this.records.get(row);
        switch(col){
            case 0:
                return rowData.nodes;
            case 1:
                return rowData.score;
            case 2:
                return rowData.champion;
        }
        return "";
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
    }
}
