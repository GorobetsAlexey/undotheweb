/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package undotheweb;

/**
 *
 * @author alexxx
 */
public class Point {
    public double x;
    public double y;
    
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    @Override
    public String toString() {
        return this.x + " : " + this.y;
    }
    
    public boolean equals(Point a) {
        return (a.x == this.x && a.y == this.y);
    }
    
    @Override
    public Point clone() {
        return new Point(this.x, this.y);
    }
}

