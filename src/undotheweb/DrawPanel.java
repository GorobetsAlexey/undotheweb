/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package undotheweb;

import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author alexxx
 */
public class DrawPanel extends JPanel {
    
    private Level level;
    
    public void setLevel(Level newLevel) {
        this.level = newLevel;
        this.updateUI();
    }
    
    
    @Override
    public void paint(Graphics g) {        
        super.paint(g);
        if (this.level != null) {
            this.level.draw(g);
        }
    }
    
    public void onMousePressedHandler(java.awt.event.MouseEvent evt) {
        if (this.level == null) {
            return;
        }
        Node selectNode = this.level.getNode(evt.getX(), evt.getY());
        if (selectNode != null) {
            this.level.currentNode = selectNode;
            this.updateUI();
        } else {
            this.level.currentNode = null;
            this.updateUI();
        }
    }
    
    public void onMouseReleasedHandler(java.awt.event.MouseEvent evt) throws WinnerException {
        if (this.level == null) {
            return;
        }
        if(this.level.isSolved()) {
            throw new WinnerException();
        }
    }
    
    public void onMouseDraggedHandler(java.awt.event.MouseEvent evt) {
        if (this.level == null || this.level.currentNode == null) {
            return;
        }
        this.level.currentNode.move(evt.getX(), evt.getY());        
        this.updateUI();
    }
}
