/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package undotheweb;

/**
 *
 * @author alexxx
 */
public class Vector {
    public double x;
    public double y;
    public double z;
    
    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;                
    }
    
    @Override
    public String toString() {
        return this.x + "; " + this.y + "; " + this.z;
    }
}
