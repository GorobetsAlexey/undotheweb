import java.util.ArrayList;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import undotheweb.*;

/**
 *
 * @author alexxx
 */
public class LevelTests {
    
    public LevelTests() {
    }
   
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
   
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void createLevelFail1() {
        new Level(2, 100, 100);
    }
    
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void createLevelFail2() {
        new Level(101, 100, 100);
    }
  
    @Test
    public void createLevelCreatesExpectedStartWeb() {
        int expectedPointsCount = 7;
        Level level = new Level(expectedPointsCount, 100, 100);
        assertEquals(level.getNodesCount(), expectedPointsCount);
        assertTrue(level.hasIntersectios());
        assertFalse(level.isSolved());
    }

   
}
