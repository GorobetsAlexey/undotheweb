

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import undotheweb.*;

/**
 *
 * @author alexxx
 */
public class MathEngineTests {
    
    public MathEngineTests() {
    }

    public MathEngine engine;

    @BeforeClass
    public static void setUpClass() throws Exception {        
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        engine = new MathEngine();
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
    
//    @Test
//    public void CheckLinesNotIntersects1() {
//        Line a = new Line(1, 1, 2, 2);
//        Line b = new Line(3, 3, 2, 2);
//        assertFalse(engine.checkLinesIntersects(a, b));
//    }
//    
//    @Test
//    public void CheckLinesNotIntersects2() {
//        Line a = new Line(1, 1, 2, 2);
//        Line b = new Line(3, 3, 4, 4);
//        assertFalse(engine.checkLinesIntersects(a, b));
//    }
//    
//    @Test
//    public void CheckLinesNotIntersects3() {
//        Line a = new Line(1, 1, 2, 2);
//        Line b = new Line(2, 1, 3, 2);
//        assertFalse(engine.checkLinesIntersects(a, b));
//    }
//    
//    @Test
//    public void CheckLinesNotIntersects4() {
//        Line a = new Line(1, 1, 1, 3);
//        Line b = new Line(2, 2, 3, 2);
//        assertFalse(engine.checkLinesIntersects(a, b));
//    }
//    
//    @Test
//    public void CheckLinesIntersects1() {
//        Line a = new Line(1, 1, 3, 3);
//        Line b = new Line(4, 3, 0, 1);
//        assertTrue(engine.checkLinesIntersects(a, b));
//    }
//    
//    @Test
//    public void CheckLinesIntersects2() {
//        Line a = new Line(1, 1, 3, 3);
//        Line b = new Line(2, 2, 0, 3);
//        assertTrue(engine.checkLinesIntersects(a, b));
//    }
//    
//    @Test
//    public void CheckLinesIntersects3() {
//        Line a = new Line(1, 1, 4, 4);
//        Line b = new Line(2, 2, 3, 3);
//        assertTrue(engine.checkLinesIntersects(a, b));
//    }
//    
//    @Test
//    public void CheckLinesIntersects4() {
//        Line a = new Line(2, 1, 2, 3);
//        Line b = new Line(1, 2, 3, 2);
//        assertTrue(engine.checkLinesIntersects(a, b));
//    }
//    
//    @Test
//    public void CheckLinesNotIntersects5() {
//        Line a = new Line(105, 61, 381, 25);
//        Line b = new Line(381, 25, 381, 246);
//        assertFalse(engine.checkLinesIntersects(a, b));
//    }
    
    @Test
    public void CheckLinesIntersects5() {
        Line a = new Line(89, 58, 381, 25);
        Line b = new Line(366, 10, 366, 40);
        assertTrue(engine.checkLinesIntersects(a, b));
    }
}
